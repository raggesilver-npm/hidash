import _ from '../src';

const obj: any = {
  a: null,
  null: {
    b: 42,
  },
  c: [{ d: 1 }, { e: 'F' }],
  f: false,
  g: undefined,
  i: 'Some text',
};

describe('_.get()', () => {
  it(`should return ${obj.a}`, () => {
    expect(_.get(obj, 'a')).toBe(obj.a);
  });

  it(`should return ${obj.a?.b}`, () => {
    expect(_.get(obj, 'a.b')).toBe(obj.a?.b);
  });

  it(`should return ${obj.c[0].d}`, () => {
    expect(_.get(obj, 'c.0.d')).toBe(obj.c[0].d);
  });

  it('should return array length', () => {
    expect(_.get(obj, 'c.length')).toBe(obj.c.length);
  });

  it(`should return ${obj.f}`, () => {
    expect(_.get(obj, 'f')).toBe(obj.f);
  });

  it(`should return ${obj['']}`, () => {
    expect(_.get(obj, '')).toBe(obj['']);
  });

  it(`should return ${obj.null.b}`, () => {
    expect(_.get(obj, 'null.b')).toBe(obj.null.b);
  });
});

describe('_.exists()', () => {
  it('should correctly identify fields', () => {
    expect(_.exists(obj, 'a')).toBe(true);
    expect(_.exists(obj, 'c.0.d')).toBe(true);
    expect(_.exists(obj, 'c.cd.d')).toBe(false);
    expect(_.exists(obj, 'f')).toBe(true);
    expect(_.exists(obj, 'g')).toBe(true);
    expect(_.exists(obj, 'h')).toBe(false);
    expect(_.exists(obj, '')).toBe(false);
  });
});

describe('_.set()', () => {
  it('should create ff.ggwp.noob', () => {
    _.set(obj, 'ff.ggwp.noob', true);
    expect(obj.ff?.ggwp?.noob).toBe(true);
  });

  it('should create zzz', () => {
    _.set(obj, 'zzz', false);
    expect(obj.zzz).toBe(false);
  });

  it('should populate array', () => {
    _.set(obj, 'c.2', -256);
    expect(obj.c[2]).toBe(-256);
  });

  it('should shrink array', () => {
    _.set(obj, 'c.length', 1);
    expect(obj.c).toHaveLength(1);
  });

  it('should expand array', () => {
    _.set(obj, 'c.length', 10);
    expect(obj.c).toHaveLength(10);
  });

  it('should throw error', () => {
    expect(() => _.set(obj, 'f.valInBoolean', true)).toThrow(
      "Cannot set property 'valInBoolean' of boolean",
    );
    expect(() => _.set(obj, 'null.b.digits', 13)).toThrow(
      "Cannot set property 'digits' of number",
    );
  });
});

describe('_.unset()', () => {
  it('should unset property', () => {
    const obj = { my: { prop: { toDelete: 42 } } };
    const key = 'my.prop.toDelete';

    expect(_.unset(obj, key)).toBe(42);
    expect(_.exists(obj, key)).toBe(false);
    expect(obj).toStrictEqual({ my: { prop: {} } });
  });

  it('should unset root property', () => {
    const obj = { my: 'life' };
    const key = 'my';

    expect(_.unset(obj, key)).toBe('life');
    expect(_.exists(obj, key)).toBe(false);
    expect(obj).toStrictEqual({});
  });

  it('should remove array item', () => {
    const obj = { list: ['my', 'stuff'] };
    const key = 'list.0';

    expect(_.unset(obj, key)).toBe('my');
    expect(obj.list).toHaveLength(1);
    expect(obj.list[0]).toBe('stuff');
  });

  it('should remove top-level array item', () => {
    const arr = ['my', 'stuff'];
    const key = '0';

    expect(_.unset(arr, key)).toBe('my');
    expect(arr).toHaveLength(1);
    expect(arr[0]).toBe('stuff');
  });

  it('should remove nested array item', () => {
    const obj = { list: [{ todo: 'yay', done: false }] };
    const key = 'list.0.done';

    expect(_.unset(obj, key)).toBe(false);
    expect(obj.list).toHaveLength(1);
    expect(obj.list[0]).toEqual({ todo: 'yay' });
  });

  it('should not throw an error on inexistent key', () => {
    const obj = { my: 'life' };
    const key = 'nope';

    const before = JSON.stringify(obj);

    expect(_.unset(obj, key)).toBeUndefined();
    expect(JSON.stringify(obj)).toEqual(before);
  });
});

describe('_.map()', () => {
  it('should return array of paths', () => {
    const map = _.map(obj);
    expect(map?.length).toBeGreaterThanOrEqual(Object.keys(obj).length);
  });

  it('should return array of paths for array base', () => {
    const arr = [2, 3, 4, { a: { b: 2, c: {} } }, 'Something'];
    const map = _.map(arr);
    expect(map).toEqual(['0', '1', '2', '3.a.b', '3.a.c', '4']);
  });

  it('should return final paths for empties', () => {
    const map = _.map({ a: { b: { c: {} } }, b: [] });
    expect(map).toEqual(['a.b.c', 'b']);
  });

  it('should not return final paths for empties', () => {
    const map = _.map({ a: { b: { c: {} } }, b: [] }, { skipEmpty: true });
    expect(map).toEqual([]);
  });

  it('should return final paths for empties + regulars', () => {
    const map = _.map({ a: { b: { c: {}, d: 3 } }, b: [] });
    expect(map).toEqual(['a.b.c', 'a.b.d', 'b']);
  });

  it('should return regulars but not final paths for empties', () => {
    const map = _.map(
      { a: { b: { c: {}, d: 3 } }, b: [] },
      { skipEmpty: true },
    );
    expect(map).toEqual(['a.b.d']);
  });

  it('should stop at arrays', () => {
    // With `stopAtArrays` set to `true` map should not return paths inside
    // arrays
    const map = _.map(obj, { stopAtArrays: true });

    expect(map).toContain('c');
    expect(map).not.toContain('c.d');
    expect(map).not.toContain('c.e');
  });
});

describe('_.diffMap()', () => {
  const baseFormData = {
    name: {
      first: null,
      last: null,
    },
    password: null,
    email: null,
  };

  const stateFormData = {
    name: {
      first: 'Mr',
      last: 'Hacker',
    },
    password: null,
    email: 'hacker@hack.hk',
    smsConfirmation: true,
    somethingNotSet: undefined,
  };

  it('should return diff map', () => {
    const map = _.diffMap(baseFormData, stateFormData);
    expect(map).toEqual(['name.first', 'name.last', 'email']);
  });

  it('should return an extended diff map', () => {
    const map = _.diffMap(baseFormData, stateFormData, { includeExtra: true });
    expect(map).toEqual([
      'name.first',
      'name.last',
      'email',
      'smsConfirmation',
    ]);
  });

  it('should return a very extended diff map', () => {
    const map = _.diffMap(baseFormData, stateFormData, {
      includeExtra: true,
      includeExtraUndefined: true,
    });
    expect(map).toEqual([
      'name.first',
      'name.last',
      'email',
      'smsConfirmation',
      'somethingNotSet',
    ]);
  });

  it('should properly compare arrays with stopAtArrays (equal)', () => {
    const a = { a: [1, 2, 3] };
    const b = { a: [1, 2, 3] };

    const res = _.diffMap(a, b, {}, { stopAtArrays: true });

    expect(res).toHaveLength(0);
  });

  it('should properly compare arrays with stopAtArrays (less items)', () => {
    const a = { a: [1, 2, 3] };
    const b = { a: [1, 2] };

    const res = _.diffMap(a, b, {}, { stopAtArrays: true });

    expect(res).toEqual(['a']);
  });

  it('should properly compare arrays with stopAtArrays (more items)', () => {
    const a = { a: [1, 2, 3] };
    const b = { a: [1, 2, 3, 4] };

    const res = _.diffMap(a, b, {}, { stopAtArrays: true });

    expect(res).toEqual(['a']);
  });

  it('should compare with ===', () => {
    jest.spyOn(JSON, 'stringify');
    const res = _.diffMap({ a: 'aaa' }, { a: 'bbb' });

    expect(res).toEqual(['a']);
    expect(JSON.stringify).not.toHaveBeenCalled();

    jest.restoreAllMocks();
  });
});
