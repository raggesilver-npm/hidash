# Hidash

A simple JavaScript library to help handle objects & arrays. Hidash makes it
easier to get, set, unset, and do much more with objects & arrays using a dot
notation similar to
[MongoDB's](https://docs.mongodb.com/v4.4/core/document/#dot-notation).

## Documentation

[Auto-generated docs.](./docs/README.md)

## Usage example

```javascript
const _ = require('@raggesilver/hidash');

const a = {
  b: { c: 21 },
  d: undefined,
  e: null,
  f: [ 42, 43 ],
};

_.get(a, 'b.c'); // 21

_.set(a, 'b.c', 42)); // 42
                      // a = { b: { c: 42 }, ... }

_.get(a, 'b.nonsense'); // undefined

_.exists(a, 'b.nonsense'); // false

_.get(a, 'd'); // undefined

_.get(a, 'e'); // null

_.exists(a, 'd'); // true

_.get(a, 'f.length'); // 2

_.map(a); // [ 'b.c', 'd', 'e', 'f.0', 'f.1' ]

// Make a copy of `a`
const b = JSON.parse(JSON.stringify(a));
// Modify something in `b`
b.b.c = 22;

_.diffMap(a, b); // [ 'b.c' ]
                 // Returns all `a` keys that exist in `b` and do not have the
                 // same value
```
