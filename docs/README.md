@raggesilver/hidash - v3.1.0

# @raggesilver/hidash - v3.1.0

## Table of contents

### Classes

- [Hidash](classes/hidash.md)

### Interfaces

- [IDiffMapConfig](interfaces/idiffmapconfig.md)
- [IMapOptions](interfaces/imapoptions.md)

### Properties

- [default](README.md#default)

## Properties

### default

• **default**: [Hidash](classes/hidash.md)
