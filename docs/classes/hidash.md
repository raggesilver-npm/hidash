[@raggesilver/hidash - v3.1.0](../README.md) / Hidash

# Class: Hidash

## Table of contents

### Constructors

- [constructor](hidash.md#constructor)

### Methods

- [diffMap](hidash.md#diffmap)
- [exists](hidash.md#exists)
- [get](hidash.md#get)
- [getOut](hidash.md#getout)
- [map](hidash.md#map)
- [set](hidash.md#set)
- [unset](hidash.md#unset)

## Constructors

### constructor

• **new Hidash**()

## Methods

### diffMap

▸ **diffMap**(`a`, `b`, `__namedParameters?`): `string`[]

Given a base object `a` and a secondary object `b`, return a map of the
fields present in both that have different values in `b` than in `a`. This
is very useful for form validation, so that you may know which fields in a
form were changed from their original values.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `a` | `Record`<string, any\> | base object |
| `b` | `Record`<string, any\> | a modified (or not) version of the base object |
| `__namedParameters` | [IDiffMapConfig](../interfaces/idiffmapconfig.md) | - |

#### Returns

`string`[]

#### Defined in

index.ts:144

___

### exists

▸ **exists**(`obj`, `path`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `Record`<string, any\> |
| `path` | `string` |

#### Returns

`boolean`

#### Defined in

index.ts:85

___

### get

▸ **get**(`obj`, `path`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `Record`<string, any\> |
| `path` | `string` |

#### Returns

`any`

#### Defined in

index.ts:18

___

### getOut

▸ `Private` **getOut**(`obj`, `path`): [`boolean`, `any`]

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `Record`<string, any\> |
| `path` | `string` |

#### Returns

[`boolean`, `any`]

#### Defined in

index.ts:30

___

### map

▸ **map**(`obj`, `__namedParameters?`): `string`[]

Return an array of strings containing the paths of every key in an object (
paths are MongoDB style dot notation strings indicating nested keys in an
object).

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `obj` | `Record`<string, any\> | An object to get the map from |
| `__namedParameters` | [IMapOptions](../interfaces/imapoptions.md) | - |

#### Returns

`string`[]

#### Defined in

index.ts:105

___

### set

▸ **set**(`obj`, `path`, `value`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `Record`<string, any\> |
| `path` | `string` |
| `value` | `unknown` |

#### Returns

`any`

#### Defined in

index.ts:35

___

### unset

▸ **unset**(`obj`, `path`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `obj` | `Record`<string, any\> |
| `path` | `string` |

#### Returns

`any`

#### Defined in

index.ts:61
