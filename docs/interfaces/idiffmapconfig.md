[@raggesilver/hidash - v3.1.0](../README.md) / IDiffMapConfig

# Interface: IDiffMapConfig

## Table of contents

### Properties

- [includeExtra](idiffmapconfig.md#includeextra)
- [includeExtraUndefined](idiffmapconfig.md#includeextraundefined)

## Properties

### includeExtra

• `Optional` **includeExtra**: `boolean`

whether or not to include paths that are present in `b` but not in `a`.

#### Defined in

index.ts:4

___

### includeExtraUndefined

• `Optional` **includeExtraUndefined**: `boolean`

whether ot not to include keys whose values are `undefined`. This only
modifies the behavior of `includeExtra`.

#### Defined in

index.ts:9
