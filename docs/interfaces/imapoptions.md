[@raggesilver/hidash - v3.1.0](../README.md) / IMapOptions

# Interface: IMapOptions

## Table of contents

### Properties

- [skipEmpty](imapoptions.md#skipempty)

## Properties

### skipEmpty

• `Optional` **skipEmpty**: `boolean`

whether or not to skip the paths for objects/arrays that are empty.

#### Defined in

index.ts:14
