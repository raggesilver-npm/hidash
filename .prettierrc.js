module.exports = {
  arrowParens: 'avoid',
  bracketSpacing: true,
  printWidth: 80,
  quoteProps: 'consistent',
  semi: true,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'all',
  useTabs: false,
};
