export interface IDiffMapConfig {
  /** whether or not to include paths that are present in `b` but not in `a`. */
  includeExtra?: boolean;
  /**
   * whether ot not to include keys whose values are `undefined`. This only
   * modifies the behavior of `includeExtra`.
   */
  includeExtraUndefined?: boolean;
}

export interface IMapOptions {
  /** whether or not to skip the paths for objects/arrays that are empty. */
  skipEmpty?: boolean;
  /** whether or not to skip mapping array elements. */
  stopAtArrays?: boolean;
}

function compareTypes(a: any, b: any): boolean {
  if (typeof a === 'object' || typeof b === 'object') {
    return JSON.stringify(a) === JSON.stringify(b);
  }
  return a === b;
}

export class Hidash {
  get(obj: Record<string, any>, path: string): any {
    const parts = path.split('.');
    let cur: any = obj;
    let i = 0;

    while (i < parts.length && cur !== undefined) {
      cur = cur?.[parts[i++]];
    }

    return cur;
  }

  private getOut(obj: Record<string, any>, path: string): [boolean, any] {
    const exists = this.exists(obj, path);
    return [exists, exists ? this.get(obj, path) : undefined];
  }

  set(obj: Record<string, any>, path: string, value: unknown): any {
    const parts = path.split('.');
    let cur: any = obj;
    let i = 0;

    for (const part of parts) {
      if (i + 1 === parts.length) {
        cur[part] = value;
        return cur[part];
      }
      if (cur[part] === undefined || cur[part] === null) {
        cur[part] = {};
      } else if (
        typeof cur[part] !== 'object' &&
        !Object.getOwnPropertyDescriptor(cur[part], parts[i + 1])?.writable
      ) {
        throw new Error(
          `Cannot set property '${parts[i + 1]}' of ${typeof cur[part]}`,
        );
      }
      cur = cur[part];
      i++;
    }
  }

  unset(obj: Record<string, any>, path: string): any {
    if (!this.exists(obj, path)) {
      return;
    }

    const parts = path.split('.');
    let cur: any = obj;
    let i = 0;

    for (const part of parts) {
      if (i + 1 === parts.length) {
        if (cur instanceof Array && /\d+/.test(part)) {
          return cur.splice(Number(part), 1)[0];
        }

        const deleted = cur[part];
        delete cur[part];
        return deleted;
      }
      cur = cur[part];
      i++;
    }
  }

  exists(obj: Record<string, any>, path: string): boolean {
    const parts = path.split('.');
    let cur = obj;
    let i = 0;

    while (i < parts.length - 1 && !!cur) {
      cur = cur[parts[i++]];
    }

    return cur !== undefined && parts[i] in cur;
  }

  /**
   * Return an array of strings containing the paths of every key in an object (
   * paths are MongoDB style dot notation strings indicating nested keys in an
   * object).
   *
   * @param obj An object to get the map from
   * @param options Options
   */
  map(
    obj: Record<string, any>,
    { skipEmpty = false, stopAtArrays = false }: IMapOptions = {},
  ): string[] {
    const res: string[] = [];

    const recursiveMap = (root: typeof obj, prefix?: string) => {
      const keys = Object.keys(root);

      if (keys.length === 0) {
        return false;
      }
      for (const k of keys) {
        const path = `${prefix ? prefix + '.' : ''}${k}`;
        if (
          typeof root[k] === 'object' &&
          root[k] !== null &&
          !(root[k] instanceof Array && stopAtArrays)
        ) {
          // Do recursion, but if no keys are added add the current path
          if (!recursiveMap(root[k], path) && !skipEmpty) {
            res.push(path);
          }
        } else {
          res.push(path);
        }
      }
      return true;
    };
    recursiveMap(obj);
    return res;
  }

  /**
   * Given a base object `a` and a secondary object `b`, return a map of the
   * fields present in both that have different values in `b` than in `a`. This
   * is very useful for form validation, so that you may know which fields in a
   * form were changed from their original values.
   *
   * @param a base object
   * @param b a modified (or not) version of the base object
   * @param options
   */
  diffMap(
    a: Record<string, any>,
    b: Record<string, any>,
    {
      includeExtra = false,
      includeExtraUndefined = false,
    }: IDiffMapConfig = {},
    mapOptions?: IMapOptions,
  ): string[] {
    const aMap = this.map(a, mapOptions);
    const res: string[] = [];

    for (const k of aMap) {
      const va = this.get(a, k);
      const [bExists, vb] = this.getOut(b, k);

      if (bExists && va !== undefined && !compareTypes(va, vb)) {
        res.push(k);
      }
    }

    if (includeExtra) {
      const bMap = this.map(b, mapOptions);
      for (const k of bMap) {
        if (
          aMap.includes(k) ||
          (this.get(b, k) === undefined && !includeExtraUndefined)
        ) {
          continue;
        }
        res.push(k);
      }
    }

    return res;
  }
}

export default new Hidash();
